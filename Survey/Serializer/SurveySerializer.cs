﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Survey.Models;

namespace Survey.Serializer
{
    public interface SurveySerializer
    {
        /// <summary>
        /// Load a survey with the specified ID.
        /// </summary>
        /// <param name="id">The survey ID.</param>
        /// <param name="language">The language.</param>
        /// <returns>A survey object.</returns>
        Survey.Models.Survey Load(int id, LanguageEnum language);
        /// <summary>
        /// Write a survey to a persistent storage.
        /// </summary>
        /// <param name="survey">The survey object to be written.</param>
        void Write(Survey.Models.Survey survey);

        /// <summary>
        /// Loads all surveys in a list.
        /// </summary>
        /// <param name="language">The language.</param>
        /// <returns>List of the surveys.</returns>
        List<Survey.Models.Survey> Load(LanguageEnum language);
    }
}
