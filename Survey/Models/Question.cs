﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Survey.Models
{
    public enum QuestionType
    {
        SingleChoice,
        MultiChoice,
        Text
    }
    public class Question
    {
        public Question()
        {

        }

        public Question(Survey survey, QuestionType type)
        {
            Survey = survey;
            SurveyId = survey.Id;
            QuestionLanguage = survey.TranslationLanguage;
            Id = NextId;
            Number = Id;
            Type = type;
            Options = new List<QuestionOption>();
        }

        public int Id { get; set; }
        [System.Xml.Serialization.XmlIgnore]
        public Survey Survey { get; set; }
        public int SurveyId { get; set; }
        public LanguageEnum QuestionLanguage { get; set; }
        public int Number { get; set; }
        public string Text { get; set; }
        public QuestionType Type { get; set; }
        public List<QuestionOption> Options { get; set; }
        public bool IsRequired { get; set; }

        private int NextId
        {
            get
            {
                if (Survey == null)
                {
                    return 1;
                }
                else
                {
                    return Survey.Questions.Count + 1;
                }
            }
        }
    }
}