﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Survey
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            string dataDirectory = AppDomain.CurrentDomain.BaseDirectory + "\\Db\\";
            AppDomain.CurrentDomain.SetData("DataDirectory", dataDirectory);

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

        }
    }
}
