﻿using Survey.Models;
using Survey.Serializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Survey.Controllers
{
    public class SurveyAnswerController : AppController
    {
        // GET: SurveyAnswer
        public ActionResult Index(int id, LanguageEnum ? language)
        {
            LanguageEnum languageDefined = LanguageEnum.English;
            if (language.HasValue)
            {
                languageDefined = language.Value;
            }

            SetCulture(languageDefined);
            
            HttpCookie cookie = Request.Cookies["Survey_" + id];
            if (cookie != null)
            {
                return RedirectToAction("AlreadyFilled");
            }

            SurveySerializer serializer = new XmlSurveySerializer();
            Survey.Models.Survey survey = serializer.Load(id, languageDefined);
            if (survey == null || survey.IsActive == false)
            {
                return RedirectToError("Survey does not exist.");
            }


            SurveyAnswer surveyAnswer = new SurveyAnswer(survey, ClientIpAddress);

            return View(surveyAnswer);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(SurveyAnswer surveyAnswer)
        {
            surveyAnswer.IpAddress = ClientIpAddress;

            SurveySerializer serializer = new XmlSurveySerializer();

            Survey.Models.Survey survey = serializer.Load(surveyAnswer.Id, LanguageEnum.English);
            if (survey == null || survey.IsActive == false)
            {
                return RedirectToError("Survey does not exist.");
            }
            SurveyAnswer answers = new SurveyAnswer(survey, surveyAnswer.IpAddress);
            answers.Language = surveyAnswer.Language;
            answers.CopyAnswers(surveyAnswer.Answers);


            foreach (QuestionAnswer a in answers.Answers)
            {
                if (a.Question.IsRequired)
                {
                    if (a.Question.Type == QuestionType.MultiChoice)
                    {
                        bool answered = false;
                        foreach (bool selected in a.MultiSelectedChoiceIds)
                        {
                            if (selected)
                            {
                                answered = true;
                                break;
                            }
                        }
                        if (!answered)
                        {
                            ModelState.AddModelError(a.Id.ToString(), Resources.Resources.Required);                            
                        }
                    }
                    if (a.Question.Type == QuestionType.SingleChoice && a.SingleSelectedChoiceId == 0)
                    {
                        ModelState.AddModelError(a.Id.ToString(), "Required");
                    }
                    if (a.Question.Type == QuestionType.Text && string.IsNullOrEmpty(a.TextValue))
                    {
                        ModelState.AddModelError(a.Id.ToString(), "Required");
                    }
                }
            }


            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Required questions are left unanswered");
                ViewBag.ModelState = ModelState;
                return View(answers);
            }

            ExcelSerializer excelSerializer = new ExcelSerializer();
            try
            {
                excelSerializer.Write(answers);
            }
            catch (Exception e)
            {
                return RedirectToError(e);
            }

            return RedirectToAction("Success", "SurveyAnswer", new { id = answers.Id, message = answers.Survey.ThankYouMessage });
        }



        [HttpGet]
        public ActionResult Success(int id, string message)
        {
            HttpCookie cookie = new HttpCookie("Survey_" + id, "True");
            cookie.Expires.AddDays(2);
            Response.SetCookie(cookie);
            ViewBag.Message = message;
            return View();
        }

        [HttpGet]
        public ActionResult AlreadyFilled()
        {
            return View();
        }
    }
}