﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace Survey.Models
{
    public class IpGeolocation
    {
        private IpGeolocation()
        {
            IpAddress = "::1";
            Country = "unknown";
            Region = "unknown";
            City = "unknown";
        }

        
        public string IpAddress { get; set; } 
        public string Country { get; set; }
        public string Region { get; set; }
        public string City { get; set; }

        public static IpGeolocation Create(string ipAddress)
        {
            IpGeolocation ipGeolocation = new IpGeolocation();
            ipGeolocation.IpAddress = ipAddress;

            XmlDocument xml = new XmlDocument();

            try
            {
                string ipGeolocationServiceUrl = string.Format("http://freegeoip.net/xml/{0}", ipAddress);
                xml.Load(ipGeolocationServiceUrl);

                XmlNode root = xml.DocumentElement;
                XmlNode countryNode = root.SelectSingleNode("/Response/CountryName");
                XmlNode regionNode = root.SelectSingleNode("/Response/RegionName");
                XmlNode cityNode = root.SelectSingleNode("/Response/City");

                ipGeolocation.Country = countryNode.InnerText;
                ipGeolocation.Region = regionNode.InnerText;
                ipGeolocation.City = cityNode.InnerText;
            }
            catch (Exception e)
            {
                ipGeolocation.Country = "exception";
                ipGeolocation.Region = "exception";
                ipGeolocation.City = "exception";
            }

            return ipGeolocation;
        }
    }
}