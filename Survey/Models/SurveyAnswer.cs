﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Survey.Models
{
    public class SurveyAnswer
    {
        public SurveyAnswer()
        {

        }

        public SurveyAnswer(Survey survey, string ip)
        {
            Survey = survey;
            Id = survey.Id;
            IpAddress = ip;
            Language = survey.TranslationLanguage;

            Answers = new List<QuestionAnswer>();
            foreach (Question q in survey.Questions)
            {
                QuestionAnswer questionAnswer = new QuestionAnswer(q);
                Answers.Add(questionAnswer);
            }
        }
        public int Id { get; set; }
        public string IpAddress { get; set; }
        public Survey Survey { get; set; }
        public LanguageEnum Language { get; set; }
        public List<QuestionAnswer> Answers { get; set; }

        public void CopyAnswers(List<QuestionAnswer> answers)
        {
            if (answers == null)
            {
                return;
            }
            for (int i = 0; i < answers.Count; i++)
            {
                Answers[i].MultiSelectedChoiceIds = answers[i].MultiSelectedChoiceIds;
                Answers[i].OtherValue = answers[i].OtherValue;
                Answers[i].SingleSelectedChoiceId = answers[i].SingleSelectedChoiceId;
                Answers[i].TextValue = answers[i].TextValue;
            }
        }
    }
}