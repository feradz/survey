﻿These are script files used to create the databases required for the Identity framework.
SQLite EF does not support DB creation. So the tables must be created before using the application.


### AspNetUsers ####

CREATE TABLE [AspNetUsers] (
    [Id]                   NVARCHAR (128) NOT NULL,
    [Email]                NVARCHAR (256) NULL,
    [EmailConfirmed]       BIT            NOT NULL,
    [PasswordHash]         NVARCHAR (256) NULL,
    [SecurityStamp]        NVARCHAR (256) NULL,
    [PhoneNumber]          NVARCHAR (256) NULL,
    [PhoneNumberConfirmed] BIT            NOT NULL,
    [TwoFactorEnabled]     BIT            NOT NULL,
    [LockoutEndDateUtc]    DATETIME       NULL,
    [LockoutEnabled]       BIT            NOT NULL,
    [AccessFailedCount]    INT            NOT NULL,
    [UserName]             NVARCHAR (256) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY ([Id] ASC)
);


GO
CREATE UNIQUE INDEX [UserNameIndex]
    ON [AspNetUsers]([UserName] ASC);

### AspNetRoles ###

CREATE TABLE [AspNetRoles] (
    [Id]   NVARCHAR (128) NOT NULL,
    [Name] NVARCHAR (256) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY ([Id] ASC)
);

GO
CREATE UNIQUE INDEX [RoleNameIndex]
    ON [AspNetRoles]([Name] ASC);


### AspNetUserRoles ###

CREATE TABLE [AspNetUserRoles] (
    [UserId] NVARCHAR (128) NOT NULL,
    [RoleId] NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY ([UserId] ASC, [RoleId] ASC),
    CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);


GO
CREATE INDEX [IX_UserId]
    ON [AspNetUserRoles]([UserId] ASC);


GO
CREATE INDEX [IX_RoleId]
    ON [AspNetUserRoles]([RoleId] ASC);


### AspNetUserLogins ###

CREATE TABLE [AspNetUserLogins] (
    [LoginProvider] NVARCHAR (128) NOT NULL,
    [ProviderKey]   NVARCHAR (128) NOT NULL,
    [UserId]        NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY ([LoginProvider] ASC, [ProviderKey] ASC, [UserId] ASC),
    CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);


GO
CREATE INDEX [IX_UserId]
    ON [AspNetUserLogins]([UserId] ASC);



### AspNetUserClaims ###

CREATE TABLE [AspNetUserClaims] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [UserId]     NVARCHAR (128) NOT NULL,
    [ClaimType]  NVARCHAR (256) NULL,
    [ClaimValue] NVARCHAR (256) NULL,
    CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY ([Id] ASC),
    CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);


GO
CREATE INDEX [IX_UserId]
    ON [AspNetUserClaims]([UserId] ASC);

