﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Survey.Models
{
    public class QuestionAnswer
    {
        public QuestionAnswer()
        {

        }
        public QuestionAnswer(Question question)
        {
            Id = question.Id;
            Question = question;
            MultiSelectedChoiceIds = new bool[question.Options.Count];
            for(int i = 0; i < MultiSelectedChoiceIds.Length; i++)
            {
                MultiSelectedChoiceIds[i] = false;
            }
        }
        public int Id { get; set; }
        public Question Question { get; set; }
        public int SingleSelectedChoiceId { get; set; }
        public bool[] MultiSelectedChoiceIds { get; set; }
        [MaxLength(255)]
        public string TextValue { get; set; }
        [MaxLength(255)]
        public string OtherValue { get; set; }

    }
}