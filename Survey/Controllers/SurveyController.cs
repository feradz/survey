﻿using Survey.Models;
using Survey.Serializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using OfficeOpenXml;

namespace Survey.Controllers
{
    [Authorize]
    public class SurveyController : AppController
    {
        // GET: Survey
        public ActionResult Index(LanguageEnum ? language)
        {
            if (!language.HasValue)
            {
                language = LanguageEnum.English;
            }
            SurveySerializer surveySerializer = new XmlSurveySerializer();
            List<Survey.Models.Survey> surveys = surveySerializer.Load(language.Value);
            return View(surveys);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Survey.Models.Survey survey)
        {
            if (!ModelState.IsValid)
            {
                return View(survey);
            }

            SurveySerializer serializer = new XmlSurveySerializer();
            try
            {
                serializer.Write(survey);
            }
            catch (Exception e)
            {
                return RedirectToError(e);
            }


            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id, LanguageEnum ? language)
        {
            if (!language.HasValue)
            {
                language = LanguageEnum.English;
            }

            SurveySerializer serializer = new XmlSurveySerializer();
            Survey.Models.Survey survey = serializer.Load(id, language.Value);
            if (survey == null)
            {
                return RedirectToError("Survey does not exists.");
            }

            return View(survey);
        }

        [HttpPost]
        public ActionResult Edit(Survey.Models.Survey survey)
        {
            SurveySerializer serializer = new XmlSurveySerializer();
            Survey.Models.Survey existingSurvey = serializer.Load(survey.Id, survey.TranslationLanguage);
            if (existingSurvey == null)
            {
                return RedirectToError("The survey does not exists.");
            }

            existingSurvey.Update(survey);
            serializer.Write(existingSurvey);
            return View(existingSurvey);
        }

        [HttpGet]
        public ActionResult AddNewTranslation(int surveyId)
        {
            SurveySerializer serializer = new XmlSurveySerializer();
            Survey.Models.Survey survey = serializer.Load(surveyId, LanguageEnum.English);
            if (survey == null)
            {
                return RedirectToError("Survey does not exists.");
            }

            List<LanguageEnum> noTranslations = survey.NotInTranslations;

            ViewBag.SurveyId = surveyId;
            return View(noTranslations);
        }

        [HttpPost]
        public ActionResult AddNewTranslation(int surveyId, LanguageEnum language)
        {
            XmlSurveySerializer serializer = new XmlSurveySerializer();
            Survey.Models.Survey translation = serializer.Load(surveyId, LanguageEnum.English);
            if (translation == null)
            {
                return RedirectToError("Survey does not exists.");
            }

            translation.TranslationLanguage = language;
            foreach (Question q in translation.Questions)
            {
                q.QuestionLanguage = language;

                foreach (QuestionOption o in q.Options)
                {
                    o.Language = language;
                }
            }

            try
            {
                serializer.Write(translation);
            }
            catch(Exception e)
            {
                return RedirectToError(e);
            }

            return RedirectToAction("Index");
        }

        public ActionResult DownloadExcel(int surveyId)
        {
            string fileName = ExcelSerializer.GetSurveyDescriptionFile(surveyId);
            if (!System.IO.File.Exists(fileName))
            {
                return RedirectToError("No excel file.");
            }

            FileInfo file = new FileInfo(fileName);
            using (ExcelPackage package = new ExcelPackage(file))
            {
                MemoryStream memStream = new MemoryStream();
                package.SaveAs(memStream);
                string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                string downloadFileName = "Survey_" + surveyId + ".xlsx";
                memStream.Position = 0;

                FileStreamResult fileStreamResult = new FileStreamResult(memStream, contentType);
                fileStreamResult.FileDownloadName = downloadFileName;
                return fileStreamResult;
            }            
        }
    }
}