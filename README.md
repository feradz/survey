By Ferad Zyulkyarov <feradz@gmail.com>

# README #

This is simple ASP.NET MVC web survey web application which does not require a database. 
The description of the survey is stored in an XML file and the survey data in an Excel file.
The survey also logs the geo-location of the user who submits the survey based on its computer's IP address.

It supports:

1. Definition of a survey.

2. Optional vs. required questions. 

3. Single choice questions with optional other field.

4. Multi-choice questions with optional other field.

5. Text-based questions

### File organization ###

The survey files are stored in directory SurveyData.

For each survey a new directory is created in SurveyData with the name of its ID.

For example ~/SurveyData/56 is directory which contains the files for survey with ID=56.

Survey IDs are generated automatically and care is taken to avoid collisions.


### Multi-Language ###

The web application supports multiple languages to describe and display the survey.

Each translation is stored in a separate file.

For example:
 ~/SurveyData/56/56_English.xml stores the description in English

 ~/SurveyData/56/56_Spanish.xml stores the description in Spanish

Results are stored in a single Excel file.