﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Survey.Serializer
{
    public static class Constants
    {
        public static readonly string SurveysPathPrefix = AppDomain.CurrentDomain.BaseDirectory + "\\SurveyData";
    }
}