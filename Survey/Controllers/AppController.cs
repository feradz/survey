﻿using Survey.Helpers;
using Survey.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Survey.Controllers
{
    public class AppController : Controller
    {
        private const string CultureCookieKey = "_culture";

        protected RedirectToRouteResult RedirectToError(Exception e)
        {
            RouteValueDictionary route = new RouteValueDictionary(new { action = "Index", controller = "Error", info = e.ToString() });
            return new RedirectToRouteResult(route);
        }

        protected RedirectToRouteResult RedirectToError(string msg)
        {
            RouteValueDictionary route = new RouteValueDictionary(new { action = "Index", controller = "Error", info = msg });
            return new RedirectToRouteResult(route);
        }

        /// <summary>
        /// Returns the client IP address.
        /// </summary>
        protected string ClientIpAddress
        {
            get
            {
                string ip = string.Empty;
                if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                {
                    ip = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                else
                {
                    ip = Request.UserHostAddress;
                }

                //
                // Remove the port
                //
                int portStartInd = ip.LastIndexOf(':');
                int dotInd = ip.IndexOf('.');
                int squareBracketInd = ip.LastIndexOf(']');
                if (dotInd > 0 && portStartInd > 0)
                {
                    //
                    // IP v.4
                    //
                    ip = ip.Substring(0, portStartInd);
                }
                else if (squareBracketInd > 0 && portStartInd > 0)
                {
                    //
                    // IP v.6
                    //
                    ip = ip.Substring(1, squareBracketInd - 1);
                }

                return ip;
            }
        }

        protected override void ExecuteCore()
        {
            string cultureName = null;
            // Attempt to read the culture cookie from Request
            HttpCookie cultureCookie = Request.Cookies[CultureCookieKey];
            if (cultureCookie != null)
            {
                cultureName = cultureCookie.Value;
            }
            else
            {
                cultureName = Request.UserLanguages[0]; // obtain it from HTTP header AcceptLanguages
            }

            // Validate culture name
            cultureName = CultureHelper.GetImplementedCulture(cultureName); // This is safe


            // Modify current thread's cultures            
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            base.ExecuteCore();
        }

        protected void SetCulture(LanguageEnum lang)
        {
            string cultureName = "en-US";
            if (lang == LanguageEnum.Bulgarian)
            {
                cultureName = "bg-BG";
            }
            if (lang == LanguageEnum.English)
            {
                cultureName = "en-US";
            }
            if (lang == LanguageEnum.Russian)
            {
                cultureName = "ru-RU";
            }
            if (lang == LanguageEnum.Spanish)
            {
                cultureName = "es-ES";
            }
            if (lang == LanguageEnum.Turkish)
            {
                cultureName = "tr-TR";
            }
            if (lang == LanguageEnum.Ukrainian)
            {
                cultureName = "uk-UA";
            }

            HttpCookie cultureCookie = Request.Cookies[CultureCookieKey];
            if (cultureCookie != null)
            {
                string preferedCulture = cultureCookie.Value;
                if (preferedCulture.ToLower() != cultureName.ToLower())
                {
                    Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
                    Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

                    cultureCookie = new HttpCookie(CultureCookieKey, cultureName);
                    Response.SetCookie(cultureCookie);
                }
            }
            else
            {
                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

                cultureCookie = new HttpCookie(CultureCookieKey, cultureName);
                Response.SetCookie(cultureCookie);
            }
        }
    }
}