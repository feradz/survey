﻿using Survey.Models;
using Survey.Serializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Survey.Controllers
{
    [Authorize]
    public class QuestionController : AppController
    {
        // GET: Question
        [HttpGet]
        public ActionResult Edit(int questionId, int surveyId, LanguageEnum ? language)
        {
            if (!language.HasValue)
            {
                language = LanguageEnum.English;
            }
            SurveySerializer serializer = new XmlSurveySerializer();
            Survey.Models.Survey survey = serializer.Load(surveyId, language.Value);
            if (survey == null)
            {
                return RedirectToError("Survey does not exist.");
            }

            Question question = null;
            if (questionId == 0)
            {
                question = new Question(survey, QuestionType.Text);
                survey.Questions.Add(question);

                try
                {
                    serializer.Write(survey);
                }
                catch (Exception e)
                {
                    return RedirectToError(e);
                }
            }
            else
            {
                question = survey.FindQuestionById(questionId);
                if (question == null)
                {
                    return RedirectToError("Question does not exist.");
                }
            }

            return View(question);
        }

        [HttpPost]
        public ActionResult Edit(Question question)
        {
            

            if (question.Type != QuestionType.Text && question.Options.Count == 0)
            {
                ModelState.AddModelError("Options", "You have not added any options.");
                return View(question);
            }


            if (question.Options != null)
            {
                int optionId = 1;
                foreach (QuestionOption option in question.Options)
                {
                    option.Id = optionId;
                    option.Language = question.QuestionLanguage;
                    option.Number = optionId;
                    option.QuestionId = question.Id;
                    optionId++;
                }

                QuestionOption lastOption = question.Options[question.Options.Count - 1];
                lastOption.IsOther = QuestionOption.DetermineIsOther(lastOption);                
            }

            SurveySerializer serializer = new XmlSurveySerializer();
            Survey.Models.Survey survey = serializer.Load(question.SurveyId, question.QuestionLanguage);
            if (survey == null)
            {
                return RedirectToError("Survey does not exist.");
            }
            survey.Update(question);

            try
            {
                serializer.Write(survey);
            }
            catch (Exception e)
            {
                return RedirectToError(e);
            }
            
            
            return RedirectToAction("Edit", "Survey", new {id = survey.Id, language = question.QuestionLanguage});
        }
    }
}