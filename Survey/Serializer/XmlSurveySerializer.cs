﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Xml.Serialization;
using Survey.Models;

namespace Survey.Serializer
{
    public class XmlSurveySerializer: SurveySerializer
    {
        public const char LanguageSeparator = '_';
        public const string SurveyFileExtension = ".xml";

        static XmlSurveySerializer()
        {
            if (!Directory.Exists(Constants.SurveysPathPrefix))
            {
                Directory.CreateDirectory(Constants.SurveysPathPrefix);
            }
        }

        public Survey.Models.Survey Load(int id, LanguageEnum language)
        {
            string inFile = GetSurveyDescriptionFile(id, language);
            if (!File.Exists(inFile))
            {
                return null;
            }
            System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(Survey.Models.Survey));
            StreamReader file = new StreamReader(inFile);
            Survey.Models.Survey survey = new Survey.Models.Survey();
            survey = (Survey.Models.Survey)reader.Deserialize(file);
            file.Close();

            return survey;
        }

        public void Write(Survey.Models.Survey survey)
        {
            if (survey.Id == 0)
            {
                survey.Id = GetNextSurveyId();
            }

            string directoryName = GetSurveyDirectory(survey);
            if (!Directory.Exists(directoryName))
            {
                Directory.CreateDirectory(directoryName);
            }

            string outFileName = GetSurveyDescriptionFile(survey);
            System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(Survey.Models.Survey));

            System.IO.StreamWriter file = new System.IO.StreamWriter(outFileName);
            writer.Serialize(file, survey);
            file.Close();
        }

        public List<Survey.Models.Survey> Load(LanguageEnum language = LanguageEnum.English)
        {
            List<Survey.Models.Survey> res = new List<Models.Survey>();
            int[] surveyIds = SurveyIds;
            foreach (int id in surveyIds)
            {
                Survey.Models.Survey survey = Load(id, language);
                if (survey != null)
                {
                    res.Add(survey);
                }
            }
            
            return res;
        }

        private static string GetSurveyDirectory(Survey.Models.Survey survey)
        {
            return GetSurveyDirectory(survey.Id);
        }

        private static string GetSurveyDirectory(int surveyId)
        {
            return Constants.SurveysPathPrefix + "\\" + surveyId + "\\";
        }

        private static string GetSurveyDescriptionFile(Survey.Models.Survey survey)
        {
            return GetSurveyDescriptionFile(survey.Id, survey.TranslationLanguage);
        }

        private static string GetSurveyDescriptionFile(int surveyId, LanguageEnum language)
        {
            return Constants.SurveysPathPrefix + "\\" + surveyId + "\\" + surveyId + LanguageSeparator + language.ToString() + SurveyFileExtension;
        }

        private static int GetNextSurveyId()
        {
            string[] directories = Directory.GetDirectories(Constants.SurveysPathPrefix);
            int nextId = 1;
            string dirNameOnly = null;
            int lastDirSeparatorInd = -1;
            foreach (string d in directories)
            {
                lastDirSeparatorInd = d.LastIndexOf(Path.DirectorySeparatorChar);
                dirNameOnly = d.Substring(lastDirSeparatorInd + 1);

                int id = Int32.Parse(dirNameOnly);
                if (id >= nextId)
                {
                    nextId = id + 1;
                }
            }

            return nextId;
        }

        private int[] SurveyIds
        {
            get
            {
                string[] surveyDirNames = Directory.GetDirectories(Constants.SurveysPathPrefix);
                int[] surveyIds = new int[surveyDirNames.Length];

                string dirNameOnly = null;
                int lastDirSeparatorInd = -1;
                int i = 0;
                foreach (string d in surveyDirNames)
                {
                    lastDirSeparatorInd = d.LastIndexOf(Path.DirectorySeparatorChar);
                    dirNameOnly = d.Substring(lastDirSeparatorInd + 1);
                    surveyIds[i] = Int32.Parse(dirNameOnly);
                    i++;
                }

                return surveyIds;
            }
        }

        public List<LanguageEnum> Translations(int surveyId)
        {
            List<LanguageEnum> translations = new List<LanguageEnum>();

            string surveyDir = GetSurveyDirectory(surveyId);
            string[] files = Directory.GetFiles(surveyDir, "*" + SurveyFileExtension);
            
            foreach (string f in files)
            {
                string lang = GetLanguageFromFileName(f);

                if (lang.ToLower() == LanguageEnum.Bulgarian.ToString().ToLower())
                {
                    translations.Add(LanguageEnum.Bulgarian);
                }
                else if (lang.ToLower() == LanguageEnum.English.ToString().ToLower())
                {
                    translations.Add(LanguageEnum.English);
                }
                else if (lang.ToLower() == LanguageEnum.Russian.ToString().ToLower())
                {
                    translations.Add(LanguageEnum.Russian);
                }
                else if (lang.ToLower() == LanguageEnum.Spanish.ToString().ToLower())
                {
                    translations.Add(LanguageEnum.Spanish);
                }
                else if (lang.ToLower() == LanguageEnum.Turkish.ToString().ToLower())
                {
                    translations.Add(LanguageEnum.Turkish);
                }
                else if (lang.ToLower() == LanguageEnum.Ukrainian.ToString().ToLower())
                {
                    translations.Add(LanguageEnum.Ukrainian);
                }
            }

            return translations;
        }

        public List<LanguageEnum> NotInTranslations(int surveyId)
        {
            List<LanguageEnum> translations = new List<LanguageEnum>();
            translations.Add(LanguageEnum.Bulgarian);
            translations.Add(LanguageEnum.English);
            translations.Add(LanguageEnum.Russian);
            translations.Add(LanguageEnum.Spanish);
            translations.Add(LanguageEnum.Turkish);
            translations.Add(LanguageEnum.Ukrainian);


            string surveyDir = GetSurveyDirectory(surveyId);
            string[] files = Directory.GetFiles(surveyDir, "*" + SurveyFileExtension);


            foreach (string f in files)
            {                
                string lang = GetLanguageFromFileName(f);

                if (lang.ToLower() == LanguageEnum.Bulgarian.ToString().ToLower())
                {
                    translations.Remove(LanguageEnum.Bulgarian);
                }
                else if (lang.ToLower() == LanguageEnum.English.ToString().ToLower())
                {
                    translations.Remove(LanguageEnum.English);
                }
                else if (lang.ToLower() == LanguageEnum.Russian.ToString().ToLower())
                {
                    translations.Remove(LanguageEnum.Russian);
                }
                else if (lang.ToLower() == LanguageEnum.Spanish.ToString().ToLower())
                {
                    translations.Remove(LanguageEnum.Spanish);
                }
                else if (lang.ToLower() == LanguageEnum.Turkish.ToString().ToLower())
                {
                    translations.Remove(LanguageEnum.Turkish);
                }
                else if (lang.ToLower() == LanguageEnum.Ukrainian.ToString().ToLower())
                {
                    translations.Add(LanguageEnum.Ukrainian);
                }
            }

            return translations;
        }

        public void CreateTranslation(int surveyId, LanguageEnum language)
        {
            string englishFile = GetSurveyDescriptionFile(surveyId, LanguageEnum.English);
            string translationFile = GetSurveyDescriptionFile(surveyId, language);

            File.Copy(englishFile, translationFile);
        }

        private string GetLanguageFromFileName(string fileName)
        {
            int startInd = 0;
            int numChars = 0;
            startInd = fileName.IndexOf(LanguageSeparator);
            numChars = fileName.Length - startInd - SurveyFileExtension.Length - 1;
            string lang = fileName.Substring(startInd + 1, numChars);
            return lang;
        }
    }

}