﻿using Survey.Serializer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Survey.Models
{
    public enum LanguageEnum
    {
        [Description("Български")]
        Bulgarian,
        [Description("English")]
        English,
        [Description("Русский")]
        Russian,
        [Description("Español")]
        Spanish,
        [Description("Türkçe")]
        Turkish,
        [Description("Украинский")]
        Ukrainian
    }
    public class Survey
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }

        public LanguageEnum TranslationLanguage { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public Dictionary<LanguageEnum, Survey> Languages { get; set; }

        [Required]
        public string Title { get; set; }        
        public string Description { get; set; }
        public string ThankYouMessage { get; set; }
        public List<Question> Questions { get; set; }


        [System.Xml.Serialization.XmlIgnore]
        public List<SurveyAnswer> Answers { get; set; }

        public Question FindQuestionById(int id)
        {
            foreach (Question q in Questions)
            {
                if (q.Id == id)
                {
                    return q;
                }
            }

            return null;
        }


        public void Update(Question newQuestion)
        {
            int questionInd = QustionIndex(newQuestion);
            Questions[questionInd] = newQuestion;
        }

        public void Update(Survey source)
        {
            Title = source.Title;
            Description = source.Description;
            ThankYouMessage = source.ThankYouMessage;
            IsActive = source.IsActive;
        }

        [System.Xml.Serialization.XmlIgnore]
        public Dictionary<LanguageEnum, string> Translations
        {
            get
            {
                XmlSurveySerializer serializer = new XmlSurveySerializer();
                List<LanguageEnum> translations = serializer.Translations(Id);
                return LanguageStrings(translations);
            }
        }

        [System.Xml.Serialization.XmlIgnore]
        public List<LanguageEnum> NotInTranslations
        {
            get
            {
                XmlSurveySerializer serializer = new XmlSurveySerializer();
                return serializer.NotInTranslations(Id);
            }
        }

        private int QustionIndex(Question question)
        {
            int questionInd = 0;
            foreach (Question q in Questions)
            {
                if (question.Id == q.Id)
                {
                    break;
                }
                questionInd++;
            }

            return questionInd;
        }

        public Dictionary<LanguageEnum, string> LanguageStrings(List<LanguageEnum> languages)
        {
            Dictionary<LanguageEnum, string> dict = new Dictionary<LanguageEnum, string>();
            foreach(LanguageEnum lang in languages)
            {
                if (lang == LanguageEnum.Bulgarian)
                {
                    dict.Add(lang, "Български");
                }
                else if (lang == LanguageEnum.English)
                {
                    dict.Add(lang, "English");
                }
                else if (lang == LanguageEnum.Russian)
                {
                    dict.Add(lang, "Русский");
                }
                else if (lang == LanguageEnum.Spanish)
                {
                    dict.Add(lang, "Español");
                }
                else if (lang == LanguageEnum.Turkish)
                {
                    dict.Add(lang, "Türkçe");
                }
                else if (lang == LanguageEnum.Ukrainian)
                {
                    dict.Add(lang, "Украинский");
                }
                
            }
            return dict;
        }
    }
}