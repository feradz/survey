﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Survey.Models
{
    public class QuestionOption
    {
        public QuestionOption()
        {

        }

        public QuestionOption(Question q)
        {
            Id = NextId;
            Question = q;
            QuestionId = q.Id;
            Number = Id;
            Language = q.QuestionLanguage;
            Text = "Other";
        }

        public int Id { get; set; }
        [System.Xml.Serialization.XmlIgnore]
        public Question Question { get; set; }
        public int Number { get; set; }
        public int QuestionId { get; set; }
        public LanguageEnum Language { get; set; }
        public string Text { get; set; }
        public bool IsOther { get; set; }

        private int NextId
        {
            get
            {
                if (Question == null)
                {
                    return 1;
                }
                else
                {
                    return Question.Options.Count + 1;
                }
            }
        }

        public static bool DetermineIsOther(QuestionOption option)
        {

            if (string.IsNullOrEmpty(option.Text))
            {
                return true;
            }

            string optionText = option.Text.ToLower();

            if (option.Language == LanguageEnum.Bulgarian && (optionText == "друг" || optionText == "другo" || optionText == "другa" || optionText == "други"))
            {
                return true;
            }
            if (option.Language == LanguageEnum.English && optionText == "other")
            {
                return true;
            }
            if (option.Language == LanguageEnum.Russian && (optionText == "другой" || optionText == "другая" || optionText == "другoе" || optionText == "другие"))
            {
                return true;
            }
            if (option.Language == LanguageEnum.Spanish && (optionText == "otro" || optionText == "otra"))
            {
                return true;
            }
            if (option.Language == LanguageEnum.Turkish && (optionText == "başka" || optionText == "diğer"))
            {
                return true;
            }
            if (option.Language == LanguageEnum.Ukrainian && (optionText == "інші" || optionText == "інший" || optionText == "інша" || optionText == "інше"))
            {
                return true;
            }

            return false;
        }
    }
}