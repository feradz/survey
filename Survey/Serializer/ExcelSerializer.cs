﻿using Survey.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;

namespace Survey.Serializer
{
    public class ExcelSerializer : SurveyAnswerSerializer
    {
        private const string WorkseetName = "SurveyResults";

        static ExcelSerializer()
        {
            _lock = new object();
        }

        public List<SurveyAnswer> Load(int sureyId)
        {
            throw new NotImplementedException();
        }

        public void Write(SurveyAnswer surveyAnswer)
        {
            string fileName = GetSurveyDescriptionFile(surveyAnswer.Id);

            if (!File.Exists(fileName))
            {
                InitializeFile(surveyAnswer);
            }

            IpGeolocation ipGeolocation = IpGeolocation.Create(surveyAnswer.IpAddress);

            FileInfo file = new FileInfo(fileName);
            lock(_lock)
            {
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[WorkseetName];
                    int lastRow = worksheet.Dimension.End.Row;
                    lastRow++;

                    //
                    // Add data
                    //
                    int col = 1;
                    foreach(QuestionAnswer a in surveyAnswer.Answers)
                    {
                        if (a.Question.Type == QuestionType.MultiChoice)
                        {
                            string value = "";
                            int lastOptionInd = a.MultiSelectedChoiceIds.Length-1;
                            for (int choiceInd = 0; choiceInd < a.MultiSelectedChoiceIds.Length; choiceInd++)
                            {
                                if (a.MultiSelectedChoiceIds[choiceInd])
                                {
                                    value = "1";
                                }
                                else
                                {
                                    value = "0";
                                }

                                //
                                // Add the Other option.
                                //
                                if (lastOptionInd == choiceInd && a.Question.Options[lastOptionInd].IsOther && a.MultiSelectedChoiceIds[lastOptionInd])
                                {
                                    value += "," + a.OtherValue;
                                }
                                worksheet.Cells[lastRow, col].Value = value;
                                col++;
                            }
                        }
                        else if (a.Question.Type == QuestionType.SingleChoice)
                        {
                            string value = a.SingleSelectedChoiceId.ToString();
                            int lastOptionId = a.Question.Options.Count - 1;
                            if (a.Question.Options[lastOptionId].IsOther && a.Question.Options[lastOptionId].Id == a.SingleSelectedChoiceId)
                            {
                                value += "," + a.OtherValue;
                            }

                            worksheet.Cells[lastRow, col].Value = value;
                            col++;
                        }
                        else if (a.Question.Type == QuestionType.Text)
                        {
                            worksheet.Cells[lastRow, col].Value = a.TextValue;
                            col++;
                        }
                    }

                    worksheet.Cells[lastRow, col].Value = surveyAnswer.IpAddress;
                    col++;
                    worksheet.Cells[lastRow, col].Value = ipGeolocation.Country;
                    col++;
                    worksheet.Cells[lastRow, col].Value = ipGeolocation.Region;
                    col++;
                    worksheet.Cells[lastRow, col].Value = ipGeolocation.City;
                    col++;
                    worksheet.Cells[lastRow, col].Value = surveyAnswer.Language;

                    package.Save();
                }
            }
        }

        public static string GetSurveyDescriptionFile(int surveyId)
        {
            return Constants.SurveysPathPrefix + "\\" + surveyId + "\\" + surveyId + ".xlsx";
        }

        public static void InitializeFile(SurveyAnswer surveyAnswer)
        {
            string fileName = GetSurveyDescriptionFile(surveyAnswer.Id);
            FileInfo file = new FileInfo(fileName);

            lock (_lock)
            {
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    //
                    // Add worksheet
                    //
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(WorkseetName);

                    //
                    // Add headers
                    //
                    int i = 1;
                    foreach (Question q in surveyAnswer.Survey.Questions)
                    {
                        string header = q.Id.ToString();
                        if (q.Type == QuestionType.MultiChoice)
                        {
                            foreach (QuestionOption option in q.Options)
                            {
                                header = q.Id.ToString() + "-" + option.Id;
                                worksheet.Cells[1, i].Value = header;
                                i++;
                            }
                        }
                        else
                        {
                            worksheet.Cells[1, i].Value = header;
                            i++;
                        }
                    }

                    worksheet.Cells[1, i].Value = "IP";
                    i++;
                    worksheet.Cells[1, i].Value = "Country";
                    i++;
                    worksheet.Cells[1, i].Value = "Region";
                    i++;
                    worksheet.Cells[1, i].Value = "City";
                    i++;
                    worksheet.Cells[1, i].Value = "Language";

                    package.Save();
                }
            }
        }

        private static readonly Object _lock;
    }
}