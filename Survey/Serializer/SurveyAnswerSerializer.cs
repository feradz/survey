﻿using Survey.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Survey.Serializer
{
    public interface SurveyAnswerSerializer
    {
        List<SurveyAnswer> Load(int sureyId);

        void Write(SurveyAnswer surveyAnswer);
    }
}